from collections import UserDict


class Field:
    pass


class Name(Field):
    def __init__(self, name):
        self.name = name

    pass


class Phone(Field):
    def __init__(self, phone):
        self.phone = phone


class Record:

    def __init__(self, name, phones):
        self.name = name
        self.phones = phones


    def delete(self,phone):
        self.phones.remove(phone)


    def add(self, phone):
        self.phones.append(phone)


    def update(self, old_phone, new_phone):
        self.phones.remove(old_phone)
        self.phones.append(new_phone)
        pass


class AddressBook(UserDict):
    def add_record(self, record):
        self.data[record.name] = record.phones


phone = Phone("0993632605")
second_phone = Phone("672542885")
record = Record("Saska", [phone])
record.add(second_phone)
record.delete(phone)
record.update(second_phone,Phone('0954015320cd'))
first_address_book = AddressBook()
first_address_book.add_record(record)

print(first_address_book['Saska'][0].phone)





# a.add()